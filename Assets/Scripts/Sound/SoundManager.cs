﻿using GameManage;
using UnityEngine;

namespace Sound
{
    public class SoundManager:MonoSingleton<SoundManager>
    {
        [SerializeField] private AudioClip startSound;
        [SerializeField] private AudioClip lV2Sound;
        [SerializeField] private AudioClip playSound;
        [SerializeField] private AudioClip laserPlayer;
        [SerializeField] private AudioClip laserEnemy;
        [SerializeField] private AudioClip explosionPlayer;
        [SerializeField] private AudioClip explosionEnemy;
        [SerializeField]private new AudioSource audio;
        public void StartSound()
        {
            audio.loop = true;
            audio.clip = startSound;
            audio.Play();
        }
        public void ClickPlayGame()
        {
            audio.clip = playSound;
            audio.loop = true;
            audio.Play();
        }

        public void Lv2Sound()
        {
            audio.clip = lV2Sound;
            audio.loop = true;
            audio.Play();
        }

        public void PlayerFire()
        {
            audio.PlayOneShot(laserPlayer);
        }

        public void EnemyFire()
        {
            audio.PlayOneShot(laserEnemy);
        }

        public void PlayerExplosion()
        {
            audio.PlayOneShot(explosionPlayer);
        }
        public void EnemyExplosion()
        {
            audio.PlayOneShot(explosionEnemy);
        }
        
    }
    
}