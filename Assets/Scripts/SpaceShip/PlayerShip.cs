﻿using GameManage;
using Sound;
using UI;
using UnityEngine;

namespace SpaceShip
{
    public class PlayerShip:BaseSpaceShip
    {
        [SerializeField] internal EnemyShip enemyShip;
        internal bool LevelUp;
        private float _collDown ;
        public PlayerShip(int hp, int speed, Bullet bullet,GameObject gameObject,Vector3 originalPosition) : base(hp,speed,bullet,gameObject,originalPosition)
        {
        }

        private void Start()
        {
            LevelUp = false;
            var position = gameObject.transform.position;
            originalPosition = new Vector3(position.x, position.y, position.z);
            HpBar.Instance.SetMaxValueCoolDown(4);
        }

        private void Update()
        {
            if (_collDown<=4)
            { 
                _collDown += Time.deltaTime;
                
            }
            HpBar.Instance.SetValueCoolDown(_collDown);
        }

        public override void Fire()
        {
            bullet.FireShip();
        }

        public void UltimateFire()
        {
            if (LevelUp&&_collDown>=4)
            {
                bullet.UltimateFireShip();
                _collDown = 0;
            }
        }

        public override void TakeHit()
        {
            hp -= enemyShip.bullet.Damage;
            if (hp<=0)
            {
                SoundManager.Instance.PlayerExplosion();
                gameObject.SetActive(false);
                gameObject.transform.position = originalPosition;
                enemyShip.gameObject.transform.position = enemyShip.originalPosition;
                LevelUp = false;
            }
        }
        private void OnTriggerEnter(Collider other)
        {
            TakeHit();
        }
    }
}

