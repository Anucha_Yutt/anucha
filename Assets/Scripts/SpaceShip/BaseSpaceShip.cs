﻿using UI;
using UnityEngine;

namespace SpaceShip
{
    public class BaseSpaceShip:MonoBehaviour
    {
        public float hp;
        public int speed;
        public Bullet bullet;
        public new GameObject gameObject;
        public Vector3 originalPosition;
        public BaseSpaceShip(int hp,int speed,Bullet bullet,GameObject gameObject,Vector3 originalPosition)
        {
            this.hp = hp;
            this.speed = speed;
            this.bullet = bullet;
            this.gameObject = gameObject;
            this.originalPosition = originalPosition;
        }
        public virtual void Fire()
        {
            //Fire Bullet
        }

        public virtual void TakeHit()
        {
            //Received the Damage
        }
    }
}