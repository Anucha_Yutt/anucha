﻿using GameManage;
using Sound;
using UI;
using UnityEngine;

namespace SpaceShip
{
    public class EnemyShip:BaseSpaceShip
    {
        [SerializeField] private double fireRate = 1;
        private float _counterFire;
        private int _scorePlayer;
        public PlayerShip playerShip;
        public EnemyShip(int hp, int speed, Bullet bullet, PlayerShip playerShip ,GameObject gameObject ,int scorePlayer,Vector3 originalPosition) : base(hp, speed, bullet,gameObject,originalPosition)
        {
            this.playerShip = playerShip;
            _scorePlayer = scorePlayer;
        }
        private void Start()
        {
            var position = gameObject.transform.position;
            originalPosition = new Vector3(position.x, position.y, position.z);
            _scorePlayer = 10;
        }

        void Update()
        {
            _counterFire += Time.deltaTime;
            if (gameObject==true)
            {
                if (_counterFire>=fireRate)
                {
                    bullet.EnemyFireShip();
                    _counterFire = 0;
                }
            }
        }

        public override void TakeHit()
        {
            hp -= playerShip.bullet.Damage;
            if (hp<=0)
            {
                SoundManager.Instance.EnemyExplosion();
                gameObject.SetActive(false);
                gameObject.transform.position = originalPosition;
            }
        }
        private void OnTriggerEnter(Collider other)
        {
            TakeHit();
            ScoreManager.Instance.SetScore(_scorePlayer);
        }
    }
}