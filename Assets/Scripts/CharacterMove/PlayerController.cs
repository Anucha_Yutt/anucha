﻿using UnityEngine;
using UnityEngine.InputSystem;
using SpaceShip;

namespace CharacterMove
{
    public class PlayerController : MonoBehaviour
    {
        public PlayerShip playerShip;
        private Vector2 _movementInput = Vector2.zero;
        private float _xMin;
        private float _xMax;
        private float _yMin;
        private float _yMax;
        private float padding = 1;
        private void Start()
        {
            SetupMoveBoundaries();
        }
        private void Update()
        {
            Move();
        }
        private void SetupMoveBoundaries()
        {
            Camera gameCamera = Camera.main;
            if (!(gameCamera is null))
            {
                _xMin = gameCamera.ViewportToWorldPoint(new Vector2(0, 0)).x + padding;
                _xMax = gameCamera.ViewportToWorldPoint(new Vector2(1, 0)).x - padding;
                _yMin = gameCamera.ViewportToWorldPoint(new Vector2(0, 0)).y + padding;
                _yMax = gameCamera.ViewportToWorldPoint(new Vector2(0, 1)).y - padding;
            }
        }
        private void Move()
        {
            var inputDirection = _movementInput.normalized;
            var inPutVelocity = inputDirection * playerShip.speed;
            var gravityForce = new Vector2(0, 0);
            var blackhole = new Vector2(0,0);
            var finalVelocity = inPutVelocity + gravityForce + blackhole;
            var position = transform.position;
            var newXPos = position.x + finalVelocity.x * Time.deltaTime;
            var newYPos = position.y + finalVelocity.y * Time.deltaTime;

            newXPos = Mathf.Clamp(newXPos, _xMin, _xMax);
            newYPos = Mathf.Clamp(newYPos, _yMin, _yMax);
            position = new Vector2(newXPos, newYPos);
            transform.position = position;
        }
        public void OnMove(InputAction.CallbackContext context)
        {
            _movementInput = context.ReadValue<Vector2>();
        }
    }
}
