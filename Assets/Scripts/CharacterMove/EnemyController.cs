﻿using SpaceShip;
using UnityEngine;


namespace CharacterMove
{
    public class EnemyController : MonoBehaviour
    {
         public EnemyShip enemyShip;
        [SerializeField] private Transform playerTransform;
        [SerializeField] private Renderer playerRenderer;
        private float chasingThresholdDistance = 1.0f;
        private Renderer _enemyRenderer;
        private void Awake()
        {
            _enemyRenderer = GetComponent<Renderer>();
        }
        void Update()
        {
            MoveToPlayer();
        }

        private void OnDrawGizmos()
        {
            CollisionDebug();
        }
        private void CollisionDebug()
        {
            if (_enemyRenderer != null && playerRenderer != null)
            {
                if (intersectAABB(_enemyRenderer.bounds,playerRenderer.bounds))
                {
                    Gizmos.color = Color.red;
                }
                else
                {
                    Gizmos.color = Color.white;
                }

                var bounds = _enemyRenderer.bounds;
                Gizmos.DrawWireCube(bounds.center, 2 * bounds.extents);
                var bounds1 = playerRenderer.bounds;
                Gizmos.DrawWireCube(bounds1.center, 2 * bounds1.extents);
            }
            
        }
        private bool intersectAABB(Bounds a,Bounds b)
        {
            return ((a.min.x <= b.max.x && a.max.x >= b.min.x) &&
                    (a.min.y <= b.max.y && a.max.y >= b.min.y));

        }
        private void MoveToPlayer()
        {
            Vector3 enemyPosition = transform.position;
            Vector3 playerPosition = playerTransform.position;
            enemyPosition.z = playerPosition.z; // ensure there is no 3D rotation by aligning Z position
            
            Vector3 vectorToTarget = playerPosition - enemyPosition; // vector from this object towards the target location
            Vector3 directionToTarget = vectorToTarget.normalized;
            Vector3 velocity = directionToTarget * enemyShip.speed;
                       
            float distanceToTarget = vectorToTarget.magnitude;

            if (distanceToTarget > chasingThresholdDistance)
            {
                transform.Translate(velocity * Time.deltaTime);
            }

        }
    }    
}

