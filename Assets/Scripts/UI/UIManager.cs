﻿using GameManage;
using UnityEngine;

namespace UI
{
    public class UIManager : MonoBehaviour
    {
         public GameObject startInterface;
         public GameObject endInterface;
         public GameObject escInterface;
         public GameObject lV2Interface;
         private bool _pressEsc;
        private void Start()
        {
            GameManager.Instance.StartGame();
            _pressEsc = false;
        }

        private void Update()
        {
            if (_pressEsc)
            {
                escInterface.gameObject.SetActive(true);
                Time.timeScale = 0;
            }
            else
            {
                ClickResume();
            }
            GameManager.Instance.OnShipExplosion();
        }

        public void ClickEsc()
        {
            _pressEsc = !_pressEsc;
        }

        public void ClickNext()
        {
            GameManager.Instance.NextLevel();
        }

        public void ClickResume()
        {
            escInterface.gameObject.SetActive(false);
            Time.timeScale = 1;
            _pressEsc = false;
        }

        protected void ClickRestart()
        {
            GameManager.Instance.Restart();
            _pressEsc = false;
        }

        protected void ClickPlay()
        {
            GameManager.Instance.Play();
            _pressEsc = false;
        }

        protected void ClickQuit()
        {
            GameManager.Instance.Quit();
            _pressEsc = false;
        }

        protected void ClickBackToMenu()
        {
            GameManager.Instance.BackToMenu();
            _pressEsc = false;
        }
    }
}