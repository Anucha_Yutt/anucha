﻿using Sound;
using UnityEngine;
using UnityEngine.Serialization;
using Random = System.Random;

namespace UI
{
    public class Bullet:MonoBehaviour
    {
        [SerializeField] private float speedBulled;
        [SerializeField]private Rigidbody bullet;
        [SerializeField]private Rigidbody ultimateBullet;
        private readonly Random _randomDamage = new Random();
        public int Damage { get;  private  set; }
        public void FireShip()
        {
            Damage = _randomDamage.Next(100);
            var transform1 = transform;
            Rigidbody bulletClone = Instantiate(bullet, transform1.position, transform1.rotation);
            bulletClone.AddForce(0, speedBulled, 0);
            SoundManager.Instance.PlayerFire();
        }
        public void UltimateFireShip()
        {
            Damage = _randomDamage.Next(800);
            var transform1 = transform;
            Rigidbody bulletClone = Instantiate(ultimateBullet, transform1.position, transform1.rotation);
            bulletClone.AddForce(0,800, 0);
            SoundManager.Instance.PlayerFire();
        }
        public void EnemyFireShip()
        {
            Damage = _randomDamage.Next(100);
            var transform1 = transform;
            Rigidbody bulletClone = Instantiate(bullet, transform1.position, transform1.rotation);
            bulletClone.AddForce(0, -speedBulled, 0);
            SoundManager.Instance.EnemyFire();
        }
    }
}