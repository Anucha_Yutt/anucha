﻿using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace GameManage
{
    public class HpBar:MonoSingleton<HpBar>
    {
          [SerializeField] private Slider hpSlider;
          [SerializeField] private Slider coolDownSlider;

         public void SetMaxValue(float hp)
         {
             hpSlider.maxValue = hp;
             hpSlider.value = hp;
         }
         public void SetValue(float hp)
         {
             hpSlider.value = hp;
         }

         public void SetMaxValueCoolDown(float fireRate)
         {
             coolDownSlider.maxValue = fireRate;
             coolDownSlider.value = fireRate;
         }

         public void SetValueCoolDown(float fireRate)
         {
             coolDownSlider.value = fireRate;
         }
    }
}