﻿using Sound;
using SpaceShip;
using UnityEngine;
using  UI;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace GameManage
{
    public class GameManager : MonoSingleton<GameManager>
    {
        [SerializeField] private PlayerShip playerShip;
        [SerializeField] private EnemyShip enemyShip;
        [SerializeField] private EnemyShip bossEnemyShip;
        [SerializeField] private EnemyShip childEnemy;
        [SerializeField] private Text textScore;
        [SerializeField] private Text scoreOnplay;
        [SerializeField] private Text winAndLose;
         public UIManager interfaceUI;
         private float _hpEnemy;
         private float _hpChildEnemy;
         private float _hpBossEnemy;
         private float _hpPlayer;
         private bool _nextLv;
         public void OnShipExplosion()
         {
             HpBar.Instance.SetValue(bossEnemyShip.hp);
             scoreOnplay.text = $"score : {ScoreManager.Instance.ShowScore()}";
             if (bossEnemyShip.hp<=500)
             {
                 childEnemy.gameObject.SetActive(true);
             }
             if (childEnemy.hp<=0)
             {
                 childEnemy.gameObject.SetActive(false);
             }
             if (enemyShip.hp<=0)
             {
                 _nextLv = true;
                 playerShip.LevelUp = true;
                 playerShip.transform.position = playerShip.originalPosition;
                 EndGame();
             }
             else if(playerShip.hp<=0)
             {
                 playerShip.LevelUp = false;
                 EndGame();
             }
             else if (bossEnemyShip.hp<=0)
             {
                 EndGame();
             }
         }
         public void StartGame()
         {
             SoundManager.Instance.StartSound();
             playerShip.gameObject.SetActive(false);
             enemyShip.gameObject.SetActive(false);
             bossEnemyShip.gameObject.SetActive(false);
             childEnemy.gameObject.SetActive(false);
             scoreOnplay.gameObject.SetActive(false);
             interfaceUI.startInterface.gameObject.SetActive(true);
             interfaceUI.endInterface.gameObject.SetActive(false);
             interfaceUI.lV2Interface.gameObject.SetActive(false);
             _hpEnemy = enemyShip.hp;
             _hpPlayer = playerShip.hp;
             _hpBossEnemy = bossEnemyShip.hp;
             _hpChildEnemy = childEnemy.hp;
             _nextLv = false;
             HpBar.Instance.SetMaxValue(_hpBossEnemy);
         }
         void EndGame()
         {
             textScore.text = $"score : {ScoreManager.Instance.ShowScore()}";
             winAndLose.text = playerShip.hp<=0 ? "L O S E" : "W I N";
             interfaceUI.endInterface.gameObject.SetActive(true);
             playerShip.gameObject.SetActive(false);
             enemyShip.gameObject.SetActive(false);
             bossEnemyShip.gameObject.SetActive(false);
             childEnemy.gameObject.SetActive(false);
             scoreOnplay.gameObject.SetActive(false);
         }
         public void Play()
         {
             SoundManager.Instance.ClickPlayGame();
             playerShip.gameObject.SetActive(true);
             enemyShip.gameObject.SetActive(true);
             bossEnemyShip.gameObject.SetActive(false);
             childEnemy.gameObject.SetActive(false);
             scoreOnplay.gameObject.SetActive(true);
             interfaceUI.startInterface.gameObject.SetActive(false);
         }
         public void Restart()
         {
             enemyShip.hp = _hpEnemy;
             playerShip.hp = _hpPlayer;
             bossEnemyShip.hp = _hpBossEnemy;
             childEnemy.hp = _hpChildEnemy;
             ScoreManager.Instance.ResetScore();
             interfaceUI.endInterface.gameObject.SetActive(false);
             interfaceUI.lV2Interface.gameObject.SetActive(false);
             Play();
         }

         public void BackToMenu()
         {
             enemyShip.hp = _hpEnemy;
             playerShip.hp = _hpPlayer;
             bossEnemyShip.hp = _hpBossEnemy;
             childEnemy.hp = _hpChildEnemy;
             ScoreManager.Instance.ResetScore();
             interfaceUI.endInterface.gameObject.SetActive(false);
             enemyShip.gameObject.transform.position = enemyShip.originalPosition;
             playerShip.gameObject.transform.position = playerShip.originalPosition;
             interfaceUI.lV2Interface.gameObject.SetActive(false);
             StartGame();
         }

         public void NextLevel()
         {
             if (_nextLv)
             {
                 bossEnemyShip.transform.position = bossEnemyShip.originalPosition;
                 playerShip.hp = _hpPlayer;
                 bossEnemyShip.hp = _hpBossEnemy;
                 enemyShip.hp = _hpEnemy;
                 enemyShip.gameObject.SetActive(false);
                 playerShip.gameObject.SetActive(true);
                 bossEnemyShip.gameObject.SetActive(true);
                 interfaceUI.lV2Interface.gameObject.SetActive(true);
                 interfaceUI.endInterface.gameObject.SetActive(false);
                 SoundManager.Instance.Lv2Sound();
                 playerShip.LevelUp = true;
             }
             
             
         }
         
         public void Quit()
         {
             Application.Quit();
         }
    }
}