﻿using UnityEngine;

namespace GameManage
{
    public class DestroyBullet:MonoBehaviour
    {
        private void OnTriggerEnter(Collider other1)
        {
            Destroy(other1.gameObject);
        }
    }
}