﻿namespace GameManage
{
    public class ScoreManager:MonoSingleton<ScoreManager>
    {
        private int Score { get; set; }

        public void SetScore(int scoreOther)
         {
             Score += scoreOther;
         }

         public int ShowScore()
         {
             return Score;
         }

         public void ResetScore()
         {
             Score = 0;
         }
    }
}